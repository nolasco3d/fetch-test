$( document ).ready(function() {
    //Verifica se é possivel usar o fetch()
    if(self.fetch){
        // console.log('yes');
        fetch('char.json')
          .then(function(response) {
              return response.json();
          })
            .then(function(data) {
            //   console.log(data.char);
                
                $('.root').append("<div class='container-fluid'><div class='row chars'></div></div>");
              for(key in data) {
                  data[key].forEach(elem => {
                      console.log(elem);
                      let name = elem.name;
                      let cls = elem.class;
                      let age = elem.age;
                      $('.chars').append("<div class='card m-4' style='width: 12em;'><img src = 'https://via.placeholder.com/150' alt = '' class= 'card-img-top'><div class='card-body'><h5 class='card-title'>" + name + "</h5><h6 class='card-subtitle text-muted'>Class: " + cls + "</h6><p class='card-text'><small>Age: " + age +"</small></p></div></div >");
                  });
              }
            });
    } 
    //Caso não seja possivel usar o fetch()
    else {
        // console.log('no');
    }
});